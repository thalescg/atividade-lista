package br.cesed.si.p3.ed.listas.exemplo1;

public class MeuArrayList {
	
	private static final int TAMANHOARRAY = 3;
	public int inseridos;	
	public Object[] arrayList = new Object[TAMANHOARRAY];
	

	public MeuArrayList() {
				
	}

	public void aumentaArray() {
		
		if(inseridos == TAMANHOARRAY) {		
			Object[] arrayTemporario = new Object[TAMANHOARRAY +3] ;
				
			for (int i = 0; i < arrayList.length; i++) {		
				
				arrayTemporario[i] = arrayList[i];				
							
				}						
			arrayList = arrayTemporario;
		}
	}		
		
	public void add(Object objeto) {
		
		aumentaArray();
		
		arrayList[inseridos] = objeto;
		inseridos ++;				
	}

	public void add(int indice, Object objeto) {
		
		aumentaArray();
		
		for (int i = inseridos; i < indice - 1 ; i--) {
			
			arrayList[i+1] = arrayList[i]; 
			
		}
		inseridos ++;
	}

	public void set(int indice, Object objeto) {
		
		arrayList[indice] = objeto;
		
	}
	
	public void remove(Object objeto) {
		
		int indice = 0; 
		boolean achou = false;
		
		for (int i = 0; i < inseridos; i++) {
		
			if(arrayList[i].equals(objeto)) {
				
				achou = true;
				indice = i;
				break;
				
			}			
		}
		
		if(achou == false) {			
			//Exception Element Not found.			
		}				
		remove(indice);				
	}

	public void remove(int indice) {
		
		for (int i = indice; i < inseridos; i++) {
			
			arrayList[i] = arrayList[i+1];
						
		}
		
		arrayList[inseridos] = null;
		inseridos --;
		
	}	
	
	public Object obterElemento(int indice) {

		return arrayList[indice];
		
	}
	
	public boolean ehVazia() {
		
		if(inseridos == 0) {			
			return true;
			
		} else {
			return false;
		}
	}
	
	public int tamanho() {
		return inseridos;
	}
	
	
}
